<?php
/**
 * @file
 * Contains install and update functions for Field Based Subscriptions.
 */

/**
 * Implements hook_schema().
 */
function field_based_subscriptions_schema() {
  $schema = array();

  $schema['field_based_subscriptions'] = array(
    'description' => 'Subscriptions storage',
    'fields' => array(
      'sid' => array(
        'description' => 'The unique ID for this particular subscription.',
        'type' => 'serial',
        'size' => 'small',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The user ID by whom this subscription belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'entity_type' => array(
        'description' => 'The subscription entity type.',
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_bundle' => array(
        'description' => 'The subscription entity bundle.',
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('sid'),
    'unique keys' => array(
      'sid' => array('sid'),
    ),
  );
  $schema['field_based_subscriptions_fields'] = array(
    'description' => 'The fields that filter a subscription',
    'fields' => array(
      'sid' => array(
        'description' => 'The unique ID for this particular subscription.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'field_name' => array(
        'description' => 'The field name.',
        'type' => 'varchar',
        'length' => '128',
        'not null' => TRUE,
        'default' => '',
      ),
      'field_value' => array(
        'description' => 'The field value.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'unique keys' => array(
      'sid_field_name' => array('sid', 'field_name',),
    ),
  );
  $schema['field_based_subscriptions_entity_queue'] = array(
    'description' => 'New items that need to be checked.',
    'fields' => array(
      'item_id' => array(
        'description' => 'The entity queue item ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'entity_id' => array(
        'description' => 'The new entity id.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'entity_type' => array(
        'description' => 'The new entity type.',
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
      ),
      'checked' => array(
        'description' => 'Boolean indicating whether this item has been checked.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'checked_timestamp' => array(
        'description' => 'The UNIX time stamp representing when the item was checked.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'item_id' => array('item_id'),
    ),
  );
  $schema['field_based_subscriptions_check_queue'] = array(
    'description' => 'The content that has been entered that needs to be checked for subscriptions.',
    'fields' => array(
      'item_id' => array(
        'description' => 'The unqiue item ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => 'The unique ID for this particular subscription.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'entity_id' => array(
        'description' => 'The unique ID of the content, such as either the {cid}, {uid}, or {nid}.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'entity_type' => array(
        'description' => 'The subscription type, such as one of "node", "comment", or "user".',
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
      ),
      'checked' => array(
        'description' => 'Boolean indicating whether this item has been checked.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'checked_timestamp' => array(
        'description' => 'The UNIX time stamp representing when the item was checked.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'item_id' => array('item_id'),
    ),
  );
  $schema['field_based_subscriptions_send_queue'] = array(
    'description' => 'The content that has been entered that needs to be checked for subscriptions.',
    'fields' => array(
      'sqid' => array(
        'description' => 'The unqiue send item ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'subscribed_uid' => array(
        'description' => 'The user ID by whom this subscription belongs to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'entity_id' => array(
        'description' => 'The unique ID of the content, such as either the {cid}, {uid}, or {nid}.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'entity_type' => array(
        'description' => 'The subscription type, such as one of "node", "comment", or "user".',
        'type' => 'varchar',
        'length' => '32',
        'not null' => TRUE,
        'default' => '',
      ),
      'sent' => array(
        'description' => 'Boolean indicating whether this item has been sent.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created_timestamp' => array(
        'description' => 'The UNIX time stamp representing when the item was created.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'sent_timestamp' => array(
        'description' => 'The UNIX time stamp representing when the item was sent.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'sqid' => array('sqid'),
    ),
  );

  return $schema;
}

